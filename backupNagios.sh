#!/bin/bash

############################################################################
# Simple backup script. It uses complete and incremental backups, with     #
# hard links to simulate snapshots. $FULL_BACKUP_LIMIT controls the        #
# frecuency of full backups.It accepts at least one source directory and a #
# single destination directory as arguments. Usage:                        #
#                                                                          #
# incremental_backup.sh SOURCE_DIRECTORY_1 [SOURCE_DIRECTORY_2..N]  	   #
#       DESTINATION_DIRECTORY                                              #
# todo: check if the log file exists. rotate
#                                                                          #
#                                                                          #
#  Author: Álvaro Reig González                                            #
#  Licence: GNU GLPv3                                                      #    
#  www.alvaroreig.com                                                      #
#  https://github.com/alvaroreig                                           #
############################################################################

# Configuration -------------------------------------------
SOURCE_DIRS="/"
#----------------------------------------------------------
FULL_BACKUP_LIMIT=7
BACKUPS_TO_KEEP=8
#----------------------------------------------------------
FILTERS=""
FILTERS="${FILTERS} --filter=include_etc/ "
FILTERS="${FILTERS} --filter=include_etc/** "
FILTERS="${FILTERS} --filter=include_home/ "
FILTERS="${FILTERS} --filter=include_home/pi/ "
FILTERS="${FILTERS} --filter=include_home/pi/** "
FILTERS="${FILTERS} --filter=include_usr/ "
FILTERS="${FILTERS} --filter=include_usr/local/ "
FILTERS="${FILTERS} --filter=include_usr/local/nagios/ "
FILTERS="${FILTERS} --filter=include_usr/local/nagios/** "
FILTERS="${FILTERS} --filter=include_usr/local/nagiosgraph/ "
FILTERS="${FILTERS} --filter=include_usr/local/nagiosgraph/** "
FILTERS="${FILTERS} --filter=include_usr/local/nagvis "
FILTERS="${FILTERS} --filter=include_usr/local/nagvis/** "
FILTERS="${FILTERS} --filter=exclude_** "
#----------------------------------------------------------
OPTIONS=""
#OPTIONS="${OPTIONS} --verbose"
OPTIONS="${OPTIONS} --archive"
OPTIONS="${OPTIONS} --human-readable"
#OPTIONS="${OPTIONS} --stats"
#OPTIONS="${OPTIONS} --dry-run"
#----------------------------------------------------------
NOTIFY=false
# Configuration END ---------------------------------------

#----------------------------------------------------------
#----------------------------------------------------------
DATE=`date +%Y%m%d` 
TIME=$(date +%H%M) 
BACKUP_PREFIX=${HOSTNAME}-backup
BACKUP_NAME=${BACKUP_PREFIX}-$DATE-$TIME

############################################################################
# Arguments processing. The last argument is the destination directory     #
############################################################################

ARGS=("$@")

if [ ${#ARGS[*]} -lt 1 ]; then
  echo "One argument is needed"
  echo "Usage: backupMyData [DESTINATION_DIR]"
  exit;
else

  #Store the destination directory
  DEST_DIR=${ARGS[${#ARGS[*]}-1]}

  echo ""
  echo ""
  echo "[" `date +%Y-%m-%d_%R` "]" "###### Starting backup #######"
  if $NOTIFY ; then
    notify-send "Starting backup"
  fi
  echo "[" `date +%Y-%m-%d_%R` "]" "Directories to backup"      $SOURCE_DIRS
  echo "[" `date +%Y-%m-%d_%R` "]" "Destination directory"      $DEST_DIR
  echo "[" `date +%Y-%m-%d_%R` "]" "Max. backups to keep:"      $BACKUPS_TO_KEEP
  echo "[" `date +%Y-%m-%d_%R` "]" "Max. full backups to keep:" $FULL_BACKUP_LIMIT
  echo "[" `date +%Y-%m-%d_%R` "]" "Filters:"                   $FILTERS
  echo ""
  echo "[" `date +%Y-%m-%d_%R` "]" "###### Browsing previous backups ######"
fi

############################################################################
# Browse previous backups                                                  #
############################################################################
BACKUPS=`ls -t $DEST_DIR |grep $BACKUP_PREFIX`
BACKUP_COUNTER=0
BACKUPS_LIST=()

for x in $BACKUPS
do
    BACKUPS_LIST[$BACKUP_COUNTER]="$x"
    echo "[" `date +%Y-%m-%d_%R` "]" "backup detected:" ${BACKUPS_LIST[$BACKUP_COUNTER]}
    let BACKUP_COUNTER=BACKUP_COUNTER+1 
    
done

############################################################################
# Delete old backups, if necessary                                         #
############################################################################

echo "[" `date +%Y-%m-%d_%R` "]" "###### Deleting old backups ######"
echo "[" `date +%Y-%m-%d_%R` "]" "Number of previous backups: " ${#BACKUPS_LIST[*]}
echo "[" `date +%Y-%m-%d_%R` "]" "Backups to keep:"             $BACKUPS_TO_KEEP

###
if [ $BACKUPS_TO_KEEP -lt ${#BACKUPS_LIST[*]} ]; then
  let BACKUPS_TO_DELETE=${#BACKUPS_LIST[*]}-$BACKUPS_TO_KEEP
  echo "[" `date +%Y-%m-%d_%R` "]" "Need to delete" $BACKUPS_TO_DELETE" backups" $BACKUPS_TO_DELETE

  while [ $BACKUPS_TO_DELETE -gt 0 ]; do
    BACKUP=${BACKUPS_LIST[${#BACKUPS_LIST[*]}-1]}
    unset BACKUPS_LIST[${#BACKUPS_LIST[*]}-1]
    echo "[" `date +%Y-%m-%d_%R` "]" "Backup to delete:" $BACKUP
    rm -rf $DEST_DIR"/"$BACKUP
    if [ $? -ne 0 ]; then
      echo "[" `date +%Y-%m-%d_%R` "]" "####### Error while deleting backup #######"
    else
      echo "[" `date +%Y-%m-%d_%R` "]" "Backup correctly deleted"
    fi
    let BACKUPS_TO_DELETE=BACKUPS_TO_DELETE-1
  done
else
  echo "[" `date +%Y-%m-%d_%R` "]" "No need to delete backups"  
fi


############################################################################
# The next backup will be complete if there is no full backup in the last  #
# FULL_BACKUP_LIMIT backups. If it is incremental, the last full backup    #
# will be used as a reference for the "--link-dest" option                 #
############################################################################

NEXT_BACKUP_FULL=true
COUNTER=0
LAST_FULL_BACKUP=

echo "[" `date +%Y-%m-%d_%R` "]" "###### Performing the backup ######"

while [[ $COUNTER -lt $FULL_BACKUP_LIMIT && $COUNTER -lt ${#BACKUPS_LIST[*]} ]]; do
  if [[ ${BACKUPS_LIST[$COUNTER]} == *full* ]]; then
  	NEXT_BACKUP_FULL=false;
  	LAST_FULL_BACKUP=${BACKUPS_LIST[$COUNTER]}
    echo "[" `date +%Y-%m-%d_%R` "]" "A full backup was performed" $COUNTER "backups ago which is less that the specified limit of" $FULL_BACKUP_LIMIT
  	break;
  fi
  let COUNTER=COUNTER+1
done

############################################################################
# Finally, the backup is performed                                         #
############################################################################

if [ $NEXT_BACKUP_FULL == true ]; then
	echo "[" `date +%Y-%m-%d_%R` "]" "The backup will be full"
  if $NOTIFY ; then
    notify-send "The backup will be full"
  fi
	rsync $OPTIONS $FILTERS $SOURCE_DIRS $DEST_DIR/$BACKUP_NAME-full
else
  echo "[" `date +%Y-%m-%d_%R` "]" "The backup will be incremental"
  if $NOTIFY ; then
    notify-send "The backup will be incremental"
  fi
	rsync $OPTIONS $FILTERS --link-dest=$DEST_DIR/$LAST_FULL_BACKUP $SOURCE_DIRS $DEST_DIR/$BACKUP_NAME-inc
fi

############################################################################
# Log the backup status                                                    #
############################################################################

if [ $? -ne "0" ]; then
	echo "[" `date +%Y-%m-%d_%R` "]" "####### Error during the backup. Please execute the script with the -v flag #######"
  if $NOTIFY ; then
    notify-send "Error during the backup"
  fi
  echo ""
  echo ""
else
  echo "[" `date +%Y-%m-%d_%R` "]" "####### Backup correct #######"
  if $NOTIFY ; then
    notify-send "Backup correct"
  fi
  echo ""
  echo ""
fi

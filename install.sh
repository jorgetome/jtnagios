#/bin/bash

# Author: Jorge Tomé Hernando <jorge@jorgetome.info>
# Date: October 2017
# Home: https://gitlab.com/jorgetome/jtnagios.git

# Description
# This scripts starts on a Debian:Latest system and...
#  * Install and configure Nagios Core
#  * Install and configure Nagios-plugins
#  * Install and configure Nagiosgraph
#  * Install and configure Nagvis (& mk-livestatus as a pre-requisite)

# Credits
# Freely based on https://github.com/loitho/Docker-Nagios-Nagvis-Nagiosgraph/blob/master/Dockerfile by loitho


# -----------------------------------------------
# Configuration

SRC_DIR='/usr/local/src'
NAGIOS_VERSION='nagios-4.3.4'
NAGIOS_PLUGINS_VERSION='nagios-plugins-2.2.1'
NAGIOSGRAPH_VERSION='1.5.2'
MKLIVESTATUS_VERSION='1.2.8'
NAGVIS_VERSION='nagvis-1.9.4'
# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# Prepare the environment

DEBIAN_FRONTEND=noninteractive
LANG=C.UTF-8

apt-get update && 
apt-get upgrade -y --no-install-recommends --quiet &&
apt-get install -y --no-install-recommends --quiet \
  apache2 \
  apache2-utils \
  build-essential \
  libapache2-mod-php \
  ssmtp \
  unzip \
  wget
# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# Nagios Core install

echo "Installing Nagios Core (${NAGIOS_VERSION})..."

apt-get install -y --no-install-recommends \
  libgd-dev \
  libpng-dev

cd "${SRC_DIR}"
wget --quiet --no-check-certificate "https://assets.nagios.com/downloads/nagioscore/releases/${NAGIOS_VERSION}.tar.gz"
tar -xzf "${NAGIOS_VERSION}.tar.gz"
cd "${NAGIOS_VERSION}"

useradd \
  --comment "nagios@`hostname`" \
  --no-create-home \
  --uid 1111 \
  --user-group \
  nagios
usermod -a -G nagios www-data

./configure \
  --with-nagios-user=nagios \
  --with-nagios-group=nagios \
  --with-command-group=nagios

make all
make install
make install-init
make install-commandmode
make install-config
make install-webconf
make install-exfoliation

echo -n admin | htpasswd -i -c "/usr/local/nagios/etc/htpasswd.users" nagiosadmin

# We have to enable the service (at least on Raspbian)
sudo update-rc.d nagios defaults

# Clean up
rm -f "${SRC_DIR}/${NAGIOS_VERSION}.tar.gz"
# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# Nagios plugins install

echo "Installing Nagios plugins (${NAGIOS_PLUGINS_VERSION})..."

apt-get install -y --no-install-recommends \
  ca-certificates \
  dnsutils \
  fping \
  libssl-dev \
  libtool \
  libwww-perl \
  libxml2-utils \
  openssl \
  ssh

cd "${SRC_DIR}"
wget --quiet --no-check-certificate "http://nagios-plugins.org/download/${NAGIOS_PLUGINS_VERSION}.tar.gz"
tar -xzf "${NAGIOS_PLUGINS_VERSION}.tar.gz"
cd "${NAGIOS_PLUGINS_VERSION}"

./configure \
  --with-nagios-user=nagios \
  --with-nagios-group=nagios \
  --with-openssl

make 
make install

ln -s /usr/local/nagios/etc /etc/nagios

# Clean up
rm -f "${SRC_DIR}/${NAGIOS_PLUGINS_VERSION}.tar.gz"
# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# Nagiosgraph install

echo "Installing Nagiosgraph (nagiosgraph-${NAGIOSGRAPH_VERSION})..."
apt-get install -y --no-install-recommends \
  libcgi-pm-perl \
  libgd-gd2-perl \
  libnagios-object-perl \
  librrds-perl

cd "${SRC_DIR}"
wget --quiet --no-check-certificate "http://downloads.sourceforge.net/project/nagiosgraph/nagiosgraph/${NAGIOSGRAPH_VERSION}/nagiosgraph-${NAGIOSGRAPH_VERSION}.tar.gz"
tar -xzf "nagiosgraph-${NAGIOSGRAPH_VERSION}.tar.gz"
cd "nagiosgraph-${NAGIOSGRAPH_VERSION}"

./install.pl --check-prereq
NG_PREFIX=/usr/local/nagiosgraph NG_WWW_DIR=/usr/local/nagios/share ./install.pl --install \
--prefix "/usr/local/nagiosgraph" \
--nagios-user nagios \
--www-user www-data

cat > /usr/local/nagiosgraph/etc/nagiosgraph-apache.conf << EOF
# enable nagiosgraph CGI scripts
ScriptAlias /nagiosgraph/cgi-bin "/usr/local/nagiosgraph/cgi"
<Directory "/usr/local/nagiosgraph/cgi">
  Options ExecCGI
  AllowOverride None
  Order allow,deny
  Allow from all
  Require all granted
</Directory>
# enable nagiosgraph CSS and JavaScript
Alias /nagiosgraph "/usr/local/nagios/share"
<Directory "/usr/local/nagios/share">
  Options None
  AllowOverride None
  Order allow,deny
  Allow from all
  Require all granted
</Directory>
EOF

cat >> /usr/local/nagios/etc/nagios.cfg << EOF

# Nagiosgraph configuration
process_performance_data=1
service_perfdata_file=/tmp/perfdata.log
service_perfdata_file_template=$LASTSERVICECHECK$||$HOSTNAME$||$SERVICEDESC$||$SERVICEOUTPUT$||$SERVICEPERFDATA$
service_perfdata_file_mode=a
service_perfdata_file_processing_interval=30
service_perfdata_file_processing_command=process-service-nagiosgraph-perfdata
# Nagiosgraph configuration - END
EOF

cat >> /usr/local/nagios/etc/objects/commands.cfg << EOF

# Nagiosgraph configuration
define command {
       command_name  process-service-nagiosgraph-perfdata
       command_line  /usr/local/nagiosgraph/bin/insert.pl
}
# Nagiosgraph configuration - END
EOF

ln -s /usr/local/nagiosgraph/etc/nagiosgraph-apache.conf /etc/apache2/sites-enabled/nagiosgraph.conf
ln -s /usr/local/nagiosgraph/etc /etc/nagiosgraph

# Clean up
rm -f "${SRC_DIR}/nagiosgraph-${NAGIOSGRAPH_VERSION}.tar.gz"
# -----------------------------------------------
# -----------------------------------------------


# -----------------------------------------------
# mk-livestatus install

echo "Installing mk-livestatus (mk-livestatus-${MKLIVESTATUS_VERSION})..."
#apt-get install -y --no-install-recommends \

cd "${SRC_DIR}"
wget --quiet --no-check-certificate "http://www.mathias-kettner.de/download/mk-livestatus-${MKLIVESTATUS_VERSION}.tar.gz"
tar -xzf "mk-livestatus-${MKLIVESTATUS_VERSION}.tar.gz"
cd "mk-livestatus-${MKLIVESTATUS_VERSION}"

./configure --with-nagios4
make
make install

echo 'broker_module=/usr/local/lib/mk-livestatus/livestatus.o /usr/local/nagios/var/rw/live'>> /usr/local/nagios/etc/nagios.cfg

# Clean up
rm -f "${SRC_DIR}/mk-livestatus-${MKLIVESTATUS_VERSION}.tar.gz"


# -----------------------------------------------
# NagVis install

echo "Installing NagVis (${NAGVIS_VERSION})..."

# Installing pre-requisites
apt-get install -y --no-install-recommends \
	graphviz \
	php-gd \
	php-gettext \
	php-json \
	php-mbstring \
	php-net-socket \
	php-sqlite3

cd "${SRC_DIR}"
wget --quiet --no-check-certificate "http://www.nagvis.org/share/${NAGVIS_VERSION}.tar.gz"
tar -xzf "${NAGVIS_VERSION}.tar.gz"
cd "${NAGVIS_VERSION}"

./install.sh \
	-n /usr/local/nagios \
	-p /usr/local/nagvis \
	-l "unix:/usr/local/nagios/var/rw/live" \
	-b mklivestatus \
	-u www-data \
	-g www-data \
	-w /etc/apache2/conf-enabled \
	-a y \
	-F -q

# Clean up
rm -f "${SRC_DIR}/${NAGVIS_VERSION}.tar.gz"


# Apache2 adjusts

a2enmod rewrite
a2enmod cgi
service apache2 restart
service nagios restart


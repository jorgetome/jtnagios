# jtnagios

`jtnagios` is a bash script aimed to install on a Debian/Ubuntu system a
complete and functional Nagios Core monitoring system plus some basic
aditional elements...

* Nagios plugins
* Nagiosgraph
* Nagvis
* mk-livestatus (required by Nagvis)

`jtnagios` is freely base in multiple scripts I have found in the Internet
but mainly in https://github.com/loitho/Docker-Nagios-Nagvis-Nagiosgraph/blob/master/Dockerfile


# Current status

- Oct 26 2017 - Nagios Core and Nagios plugins installed, configured and running over `localhost`.
- Oct 26 2017 - Nagios Core, Plugins and Nagiosgraph installed, configured and running


# Post-install tasks

After the execution of the installation script you will get a running Nagios 
system, but there are some aditional tasks that you should do in order to
finish the system.

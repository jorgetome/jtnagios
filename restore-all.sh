#!/bin/bash

############################################################################
# Simple backup script. It uses complete and incremental backups, with     #
# hard links to simulate snapshots. $FULL_BACKUP_LIMIT controls the        #
# frecuency of full backups.It accepts at least one source directory and a #
# single destination directory as arguments. Usage:                        #
#                                                                          #
# restore-all.sh SOURCE_DIRECTORY  	                                   #
#                                                                          #
#  License: GNU GLPv3                                                      #    
#  www.jorgetome.info							   #
############################################################################


# Configuration -------------------------------------------
DESTINATION_DIR="/"
#----------------------------------------------------------
FILTERS=""
FILTERS="${FILTERS} --filter=exclude_etc"
#----------------------------------------------------------
OPTIONS=""
OPTIONS="${OPTIONS} --archive"
OPTIONS="${OPTIONS} --inplace"
OPTIONS="${OPTIONS} --stats"
OPTIONS="${OPTIONS} --human-readable"
#OPTIONS="${OPTIONS} --verbose"
#OPTIONS="${OPTIONS} --progress"
#OPTIONS="${OPTIONS} --dry-run"
# Configuration END ---------------------------------------


############################################################################
# Arguments processing. The last argument is the destination directory     #
############################################################################

ARGS=("$@")

if [ ${#ARGS[*]} -lt 1 ]; then
  echo "One argument is needed"
  echo "Usage: $0 [BACKUP_DIR]"
  exit -1;
else
  #Store the source directory
  SOURCE_DIR=${ARGS[${#ARGS[*]}-1]}

  echo ""
  echo ""
  echo "[" `date +%Y-%m-%d_%R` "]" "###### Starting restore #######"
  echo "[" `date +%Y-%m-%d_%R` "]" "Backup to restore" $SOURCE_DIR
  echo "[" `date +%Y-%m-%d_%R` "]" "Restore destination" $DESTINATION_DIR
fi

rsync $OPTIONS $FILTERS $SOURCE_DIR $DESTINATION_DIR

############################################################################
# Log the restore status                                                   #
############################################################################

if [ $? -ne "0" ]; then
  echo "[" `date +%Y-%m-%d_%R` "]" "####### Error during the restore. Please execute the script with the -v flag #######"
else
  echo "[" `date +%Y-%m-%d_%R` "]" "####### Restore correct #######"
fi
echo ""
echo ""
